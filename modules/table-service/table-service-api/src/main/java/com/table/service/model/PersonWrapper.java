/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.table.service.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Person}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Person
 * @generated
 */
@ProviderType
public class PersonWrapper implements Person, ModelWrapper<Person> {
	public PersonWrapper(Person person) {
		_person = person;
	}

	@Override
	public Class<?> getModelClass() {
		return Person.class;
	}

	@Override
	public String getModelClassName() {
		return Person.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("personId", getPersonId());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long personId = (Long)attributes.get("personId");

		if (personId != null) {
			setPersonId(personId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}
	}

	@Override
	public Person toEscapedModel() {
		return new PersonWrapper(_person.toEscapedModel());
	}

	@Override
	public Person toUnescapedModel() {
		return new PersonWrapper(_person.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _person.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _person.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _person.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _person.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Person> toCacheModel() {
		return _person.toCacheModel();
	}

	@Override
	public int compareTo(Person person) {
		return _person.compareTo(person);
	}

	@Override
	public int hashCode() {
		return _person.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _person.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new PersonWrapper((Person)_person.clone());
	}

	/**
	* Returns the first name of this person.
	*
	* @return the first name of this person
	*/
	@Override
	public java.lang.String getFirstName() {
		return _person.getFirstName();
	}

	/**
	* Returns the last name of this person.
	*
	* @return the last name of this person
	*/
	@Override
	public java.lang.String getLastName() {
		return _person.getLastName();
	}

	/**
	* Returns the uuid of this person.
	*
	* @return the uuid of this person
	*/
	@Override
	public java.lang.String getUuid() {
		return _person.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _person.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _person.toXmlString();
	}

	/**
	* Returns the person ID of this person.
	*
	* @return the person ID of this person
	*/
	@Override
	public long getPersonId() {
		return _person.getPersonId();
	}

	/**
	* Returns the primary key of this person.
	*
	* @return the primary key of this person
	*/
	@Override
	public long getPrimaryKey() {
		return _person.getPrimaryKey();
	}

	@Override
	public void persist() {
		_person.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_person.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_person.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_person.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_person.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the first name of this person.
	*
	* @param firstName the first name of this person
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_person.setFirstName(firstName);
	}

	/**
	* Sets the last name of this person.
	*
	* @param lastName the last name of this person
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_person.setLastName(lastName);
	}

	@Override
	public void setNew(boolean n) {
		_person.setNew(n);
	}

	/**
	* Sets the person ID of this person.
	*
	* @param personId the person ID of this person
	*/
	@Override
	public void setPersonId(long personId) {
		_person.setPersonId(personId);
	}

	/**
	* Sets the primary key of this person.
	*
	* @param primaryKey the primary key of this person
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_person.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_person.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the uuid of this person.
	*
	* @param uuid the uuid of this person
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_person.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PersonWrapper)) {
			return false;
		}

		PersonWrapper personWrapper = (PersonWrapper)obj;

		if (Objects.equals(_person, personWrapper._person)) {
			return true;
		}

		return false;
	}

	@Override
	public Person getWrappedModel() {
		return _person;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _person.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _person.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_person.resetOriginalValues();
	}

	private final Person _person;
}