create table PersonTable_Person (
	uuid_ VARCHAR(75) null,
	personId LONG not null primary key,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null
);