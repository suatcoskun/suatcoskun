/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.table.service.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.Date;
import java.util.List;

import com.table.service.exception.NoSuchPersonException;
import com.table.service.model.Person;
import com.table.service.service.base.PersonLocalServiceBaseImpl;

/**
 * The implementation of the person local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.table.service.service.PersonLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PersonLocalServiceBaseImpl
 * @see com.table.service.service.PersonLocalServiceUtil
 */
@ProviderType
public class PersonLocalServiceImpl extends PersonLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.table.service.service.PersonLocalServiceUtil} to access the person local service.
	 */
	
	public Person addPerson(String firstName, String lastName){
		long personId=counterLocalService.increment();
		
		Person person=personPersistence.create(personId);
		person.setFirstName(firstName);
		person.setLastName(lastName);	
		//person.setBirthDate(birthDate);
		return personPersistence.update(person);	
	}
	
	public List<Person> getAllPersons(){
		
		return personPersistence.findAll();
	}
	
	public void removePersons(){
		
		personPersistence.removeAll();
	}
	
	public void removePerson(long personId) throws NoSuchPersonException{
		
		
		personPersistence.remove(personId);
	}
	
	public Person updatePerson(long personId, String firstName, String lastName) throws NoSuchPersonException{
		
		Person person= personPersistence.findByPrimaryKey(personId);
		
		person.setFirstName(firstName);
		person.setLastName(lastName);
		
		return personPersistence.update(person);
		
		
	}
	
}