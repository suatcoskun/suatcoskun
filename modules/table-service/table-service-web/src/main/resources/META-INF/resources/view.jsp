<%@page import="java.util.List"%>
<%@ include file="/init.jsp" %>
<%@ page import="com.table.service.model.Person" %>



<portlet:actionURL name="addPerson" var="addPersonUrl"/>
<portlet:actionURL name="removePerson" var="removePersonUrl"/>
<portlet:actionURL name="updatePerson" var="updatePersonUrl"/>
<portlet:resourceURL id="loadPersons" var="loadPersonsUrl"/>
<portlet:actionURL name="removeAllPersons" var="removeAllPersonsUrl"/>
<%-- <form id="loadPersonForm" action="${loadPersonsUrl}" method="get"/>
 --%>
 
	<form action="${addPersonUrl}" method="post">
	  <div class="form-group">
	    <label for="firstName">Vorname</label>
	    <input type="text" class="form-control" name="<portlet:namespace/>firstName" id="firstName" aria-describedby="firstNamelHelp" placeholder="Vorname">
	    <small id="firstNamelHelp" class="form-text text-muted">Bitte Vorname eingeben</small>
	  </div>
	
	    <div class="form-group">
	    <label for="lastName">Nachname</label>
	    <input type="text" class="form-control"  name="<portlet:namespace/>lastName" id="lastName" aria-describedby="lastNamelHelp" placeholder="Nachname">
	    <small id="lastNameHelp" class="form-text text-muted">Bitte Nachname eingeben</small>
	  </div>
	
	  <button type="submit" class="btn btn-primary">Hinzuf�gen</button>  
	</form>
	<br>
	
	<%
	List<Person> persons =(List<Person>) request.getAttribute("persons");
	%>
	
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">${numberOfPersons} Personen</div>
	
  <!-- Table -->
	 <table class="table">
		<thead>
	     <tr>
	     	<th>#</th>
	       <th>Vorname</th>
	       <th>Nachname</th>  
	     </tr>
	   </thead>
	   <tbody>
		<c:forEach items="${persons}" var="person" varStatus="loop">
	    	<tr>
	    		<th scope="row" class="index" data-person-id="${person.getPersonId()}" data-first-name="${person.getFirstName()}" data-last-name="${person.getLastName()}">
	    		<a href="#">${loop.index+1}</a></th>
	         	<td ><c:out value="${person.getFirstName()}" /></td>
	      		<td ><c:out value="${person.getLastName()}" /></td>
	      		
		    </tr>
		</c:forEach>           
	   </tbody>
	 </table> 
	</div>
	
	<nav class="text-center">
	    <ul class="pagination">
	        <li class="pag_prev">
	            <a href="#" aria-label="Previous">
	                <span aria-hidden="true">&laquo;</span>
	            </a>
	        </li>
	        <li class="pag_next">
	            <a href="#" aria-label="Next">
	                <span aria-hidden="true">&raquo;</span>
	            </a>
	        </li>
	    </ul>
	</nav>






   <!-- Modal -->
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h4 class="modal-title" id="myModalLabel">Person bearbeiten</h4>
         </div>
         <div class="modal-body">
         <form id="modalForm" action="${updatePersonUrl}" method="post">
         
           <h1>Wollen Sie die Person l�schen?</h1>
           <h6>Name:</h6>  <input id="modalFirstName" name="<portlet:namespace/>modalFirstName" type="text" value=""/>
           <h6>Nachname:</h6>  <input id="modalLastName" name="<portlet:namespace/>modalLastName" type="text" value=""/>
         </div>
         <div class="modal-footer">
         	<input type="text" class="hidden" name="<portlet:namespace/>modalPersonId" id="modalPersonId"/>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
           <button type="button" class="btn btn-primary" id="modalRemove"  >Person l�schen</button>
           <button type="button" class="btn btn-primary"  id="modalUpdate" >Person aktualisieren</button>
           
         </div>
         </form>
         
       </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
<script>
var modalForm=$('#modalForm');
$('#modalRemove').click(function(){	
	$('#modalForm').attr("action","${removePersonUrl}")	;
	modalForm.submit();
	
});
$('#modalUpdate').click(function(){
	$('#modalForm').attr("action","${updatePersonUrl}");
	modalForm.submit();
});


</script>