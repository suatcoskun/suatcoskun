$(function(){
	
	$("#myModal").modal("hide");
	$("#myModal").hide();
	$('.modal-backdrop').hide();
	$("body").removeClass("modal-open");
	
	
	$('.index').click(function(e){
		e.preventDefault();
		var that=$(this);
		var personId=that.data('personId');
		var fistName=that.data('firstName');
		var lastName=that.data('lastName');
		$("#modalPersonId").val(personId);
		$('#modalFirstName').val(fistName);
		$('#modalLastName').val(lastName);
		$('#myModal').modal('show');
		  e.stopPropagation();
	})
	
	
	
// servesource&ajax&JS	
	
//	var loadPersonFormURL=$("#loadPersonForm").attr('action');
//	var tbody=$("tbody");
//	
//	
//	$.ajax({
//		dataType:"json",
//		type:"GET",
//		url:loadPersonFormURL,
//		cache:false
//	}).done(function(data){
//		console.log(data);
//		$.each(data,function(index,value){
//			var firstName=value.firstName;
//			var lastName=value.lastName;
//			var personId=value.personId;
//			var index=index+1;
//			console.log(firstName,lastName,personId);
//
//			$(tbody).append('<tr><th scope="row" class="index" data-person-id="'+personId+'" data-first-name="'+firstName+'" data-last-name="'+lastName+'" <a href="#">' + index +'</a></th><td>'+ firstName+' </td> <td>'+ lastName+' </td></tr>');
//		
//		})
//	}).fail(function(error){
//		console.log(error)
//	})
//	
	
	//pagination
	pageSize = 10;
	pagesCount = $("tbody>tr").length;
	var currentPage = 1;

	/////////// PREPARE NAV ///////////////
	var nav = '';
	var totalPages = Math.ceil(pagesCount / pageSize);
	for (var s=0; s<totalPages; s++){
	    nav += '<li class="numeros"><a href="#">'+(s+1)+'</a></li>';
	}
	$(".pag_prev").after(nav);
	$(".numeros").first().addClass("active");
	//////////////////////////////////////

	showPage = function() {
	    $("tbody>tr").hide().each(function(n) {
	        if (n >= pageSize * (currentPage - 1) && n < pageSize * currentPage)
	            $(this).show();
	    });
	}
	showPage();


	$(".pagination li.numeros").click(function(e) {
		e.preventDefault();
	    $(".pagination li").removeClass("active");
	    $(this).addClass("active");
	    currentPage = parseInt($(this).text());
	    showPage();
	});

	$(".pagination li.pag_prev").click(function(e) {
		e.preventDefault();
	    if($(this).next().is('.active')) return;
	    $('.numeros.active').removeClass('active').prev().addClass('active');
	    currentPage = currentPage > 1 ? (currentPage-1) : 1;
	    showPage();
	});

	$(".pagination li.pag_next").click(function(e) {
		e.preventDefault();

	    if($(this).prev().is('.active')) return;
	    $('.numeros.active').removeClass('active').next().addClass('active');
	    currentPage = currentPage < totalPages ? (currentPage+1) : totalPages;
	    showPage();
	});

})