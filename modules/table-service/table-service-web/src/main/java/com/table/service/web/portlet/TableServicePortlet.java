package com.table.service.web.portlet;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.PortalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.table.service.exception.NoSuchPersonException;
import com.table.service.model.Person;
import com.table.service.service.PersonLocalServiceUtil;
import com.table.service.web.routes.PortletNames;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.suat",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=TableServicePortlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.expiration-cache=1",
		"com.liferay.portlet.header-portlet-css=/tableperson.css",
		"com.liferay.portlet.footer-portlet-javascript=/tableperson.js",
		"javax.portlet.name="+PortletNames.PORTLET_NAME_PERSON_TABLE
		},
	service = Portlet.class
)
public class TableServicePortlet extends MVCPortlet {
	
public static final Logger log=LoggerFactory.getLogger(TableServicePortlet.class);	
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub		
            try {
           int numberOfPersons= 	PersonLocalServiceUtil.getPersonsCount();
           renderRequest.setAttribute("numberOfPersons", PersonLocalServiceUtil.getPersonsCount());
           List<Person> persons= PersonLocalServiceUtil.getAllPersons();
           renderRequest.setAttribute("persons", persons);
	} catch (Exception e) {
		// TODO: handle exception
	}	
		super.render(renderRequest, renderResponse);
	}
	
	
	

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub
		
        List<Person> persons= PersonLocalServiceUtil.getAllPersons();
        final String resourceId=resourceRequest.getResourceID();
		if (resourceId.equalsIgnoreCase("loadPersons")) {
			resourceResponse.getWriter().write(JSONFactoryUtil.looseSerializeDeep(persons));
		}
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	
	public void addPerson(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException{
			String firstName=ParamUtil.getString(actionRequest, "firstName");
			
			String lastName=ParamUtil.getString(actionRequest, "lastName");	

			
			if (!StringUtils.isBlank(firstName) && !StringUtils.isBlank(lastName)) {
				log.info("addPerson add person firstName: {} lastName: {}",firstName, lastName);
				PersonLocalServiceUtil.addPerson(firstName,lastName);
			}
			

			String defaultPage=PropsUtil.get("company.default.home.url");
			actionResponse.sendRedirect(defaultPage);

	}
	
	public void removeAllPersons(ActionRequest actionRequest,ActionResponse actionResponse){
		
		PersonLocalServiceUtil.removePersons();
	}
	public void removePerson(ActionRequest actionRequest, ActionResponse actionResponse) throws NoSuchPersonException, IOException{	
		long personId=ParamUtil.getLong(actionRequest, "modalPersonId");
		PersonLocalServiceUtil.removePerson(personId);
		log.info("removePerson remove person with id: {}", personId);
		String defaultPage=PropsUtil.get("company.default.home.url");
		actionResponse.sendRedirect(defaultPage);

	}
	public void updatePerson(ActionRequest actionRequest, ActionResponse actionResponse) throws NoSuchPersonException, IOException{
		
		long personId=ParamUtil.getLong(actionRequest, "modalPersonId");
		String firstName=ParamUtil.getString(actionRequest, "modalFirstName");
		String lastName=ParamUtil.getString(actionRequest, "modalLastName");
		log.info("updatePerson, update firstName: {} and lastName: {}", firstName, lastName);
		PersonLocalServiceUtil.updatePerson(personId, firstName, lastName);

		String defaultPage=PropsUtil.get("company.default.home.url");
		actionResponse.sendRedirect(defaultPage);

	}
	
	
	
	
}