package com.table.service.web.routes;

import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;

import java.util.logging.Logger;

import org.osgi.service.component.annotations.Component;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;
@Component(
 property = {
 "com.liferay.portlet.friendly-url-routes=META-INF/routes/routes.xml",
 "javax.portlet.name="+PortletNames.PORTLET_NAME_PERSON_TABLE
 },
 service = FriendlyURLMapper.class
)

public class TablePersonRoutesMapper extends DefaultFriendlyURLMapper {

	private static final  String MAPPING="table";

	@Override
	public String getMapping() {
		// TODO Auto-generated method stub
		return MAPPING;
	}
	
	
	

}
